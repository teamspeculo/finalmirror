angular
		.module('app')
		.controller('LogoutController', ["$scope", "$location", "$resource", "$sessionStorage", "User",

		function LogoutController($scope, $location, $resource, $sessionStorage, User)
		{
			$scope.name = "test";
			var Logout = $resource('logout');
			$scope.logout = function()
			{
				User.name = '';
				$sessionStorage.empty();
				$location.path('/');

			}
		}
]);
