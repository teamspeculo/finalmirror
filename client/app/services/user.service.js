angular
		.module('app')
		.service('User', ["$resource", function($resource) {
			this.logged_in = false;
			
			this.setEmail = function (email) {
				this.email = email;
			}
			this.login = function () {
				console.log("logging in");
				
				this.logged_in = true;
			}
			this.logout = function () {
				console.log("logging out");
				
				this.logged_in = false;
			}
		}]);