angular
		.module('app')
		.controller('SignupController', ["$scope", "$resource", "$location", "User", 

		function SignupController($scope, $resource, $location, User)
		{
			$scope.name = "test"
			var Signup = $resource('signup');
			$scope.signup = function()
			{
				
				var signup = new Signup();
				signup.email = $scope.email;
				signup.password = $scope.password;
				signup.$save(function(user) {
					console.log(user);
					$scope.name = user.local.email;
					User.setEmail(user.local.email);
					$location.path('/profile');
				});
			}
		}
]);
