angular
		.module('app')
		.controller('ToggleMirrorController', ["$scope", "$resource", "$http", "User",

		function ToggleMirrorController($scope, $resource, $http, User)
		{
			$scope.username = User.email;


			$scope.news = false;
			$scope.weather = false;
			$scope.clock = false;

			$scope.checkboxModel = {
				weatheron : false,
				newson : false,
				clockon : false
			};



			$scope.save = function()
			{
				var URL = '/mirror/getMirrors/' + $scope.username;
				var localRequest = {
					method: 'GET',
					url: URL
				};

				//pickup data
				$http(localRequest)
					.then(function(res) {
						var mirror = res.data;
						mirror.weather.on = $scope.checkboxModel.weatheron;
						mirror.news.on = $scope.checkboxModel.newson;
						mirror.clock.on = $scope.checkboxModel.clockon;

						var submit = {
							weatheron: mirror.weather.on,
							weatherx: mirror.weather.xPos,
							weathery: mirror.weather.yPos,
							newson: mirror.news.on,
							newsx: mirror.news.xPos,
							newsy: mirror.news.yPos,
							clockon: mirror.clock.on,
							clockx: mirror.clock.xPos,
							clocky: mirror.clock.yPos
						};
						console.log(mirror);

						var URL = '/mirror/update';
						var localRequest = {
							method: 'POST',
							url: URL,
							data: submit
						};

						//pickup data
						$http(localRequest)
							.then(function(res) {
								console.log(res);
							})

					})

			}
		}])

		angular.module('app').controller('ClaimMirrorController', ["$scope", "User", "$http",

				function ClaimMirrorController($scope, User, $http)
				{

					$scope.save = function()
					{
						if($scope.serial != undefined) {
							var newMirror = {
								serial : $scope.serial,
								user : User.email,
								 weather	 : {
						          on   : false,
						          xPos : 0,
						          yPos : 0
						        },
						        news		 : {
						          on   : false,
						          xPos : 0,
						          yPos : 0
						        },
						        clock		 : {
						          on   : false,
						          xPos : 0,
						          yPos : 0
						        }
							};

							var URL = '/mirror/create';
							var localRequest = {
								method: 'POST',
								url: URL,
								data: newMirror
							};

							//pickup data
							$http(localRequest)
								.then(function(res) {
									console.log(res);
								})

						}
					}


				}]);
