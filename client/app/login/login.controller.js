angular
		.module('app')
		.controller('LoginController', ["$scope", "$location", "$resource", "User",

		function LoginController($scope, $location, $resource, User)
		{
			$scope.name = "test";
			var Login = $resource('login');
			$scope.login = function()
			{

				var login = new Login();
				login.email = $scope.email;
				login.password = $scope.password;
				login.$save(function(user, putResponseHeaders) {

					$scope.name = user.local.email;
					User.setEmail(user.local.email);
					User.login();
					$location.path('/profile');
				});

			}
		}
]);
