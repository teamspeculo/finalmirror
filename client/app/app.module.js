angular
	.module('app', ['ui.bootstrap', 'ui.router', 'ngResource', 'laneolson.ui.dragdrop', 'swxSessionStorage'])


	.service('CustomMirror', ["$resource", "User", function($resource, User) {
			var UsersCustomMirror = $resource('mirror/getMirrors/:user',
				{user:'@user'});

				var test = "bob";
				var Service = function() {
					this.clock = {};
					this.weather = {};
					this.news = {};
					this.setClock = function(x, y) {
						this.clock.xPos = x;
						this.clock.yPos = y;
					}
					this.setNews = function(x, y) {
						this.news.xPos = x;
						this.news.yPos = y;
					}
					this.setWeather = function(x, y) {
						this.weather.xPos = x;
						this.weather.yPos = y;
					}
					var weatherx, weathery, clockx, clocky, newsx, newsy;
					UsersCustomMirror.get({user:User.email}, function(mirror) {

						weatherx = mirror.weather.xPos;
						weathery = mirror.weather.yPos;

						newsx = mirror.news.xPos;
						newsy = mirror.news.yPos;
						console.log("Service is getting instantiated" + newsx + " " + newsy);

						clockx = mirror.clock.xPos;
						clocky = mirror.clock.yPos;


				}).$promise.then(function() {
					this.weather = {xPos: weatherx, yPos: weathery};
					this.clock = {xPos: clockx, yPos: clocky};
					this.news = {xPos: newsx, yPos: newsy};
					console.log("And clock.xPos is " + this.clock.xPos);
				});

		}
		return new Service();
	}])


	.config(function($stateProvider, $urlRouterProvider)
	{
		$urlRouterProvider.when('', '/');
		$stateProvider
		.state('home', {
			name: 'home',
			url: '/',
			templateUrl: 'views/partial-home.html'
			})

		.state('login', {
			name: 'login',
			url: '/login',
			views: {
				"" : {
					templateUrl: 'views/login-partial.html',
					controller: 'LoginController'
				}
			}
		})


		.state('logout', {
			name: 'logout',
			url: '/logout',
			views: {
				"" : {
					templateUrl: '',
					controller: 'LogoutController'
				}
			}
		})

		.state('claim-mirror', {
			name: 'claim-mirror',
			url: '/claim',
			views: {
				"" : {
					templateUrl: 'custom-mirror/partial-claim-mirror.html',
					controller: 'ClaimMirrorController'
				}
			}
		})

		.state('customize-mirror', {
			name: 'customize-mirror',
			url: '/customize',
			views: {
				"" : {
					templateUrl: 'mirror/customize-mirror.html',
					//controller: ''
				}
			}
		})

		.state('toggle-components', {
			name: 'toggle-components',
			url: '/toggle',
			views: {
				"" : {
					templateUrl: 'custom-mirror/toggle-components.html',
					controller: 'ToggleMirrorController'
				}
			}
		})

		.state('profile', {
			name: 'profile',
			url: '/profile',
			views: {
				"" : {
					templateUrl: 'views/profile.html',
					controller: 'ProfileController'
				}
			}
		})

		.state('signup', {
			name: 'signup',
			url: '/signup',
			views: {
				"" : {
					templateUrl: 'views/partial-signup.html',
					controller: 'SignupController'
				}
			}

			});
	})

	.directive("clockDragDrop", ["CustomMirror", function(CustomMirror) {//Test function, only for getting the x and y coordinates of elements when you drag them
		return {
		restrict: 'E',
		transclude: true,
		//scope: {},
		controller: ['$scope', '$interval', function ClockController($scope, $interval)
		{

		    var tick = function(){
		      $scope.clock = Date.now();
		    }
		    tick();
		    $interval(tick, 1000);
		}],
		templateUrl: "mirror/customclock/clock.html",
		link: function link(scope, element, attrs) {

			function clockListener(){
				var drag = $(this).data('draggabilly');
				var x = drag.position.x;
				var y = drag.position.y;
				console.log('dragMove happened', x, y);
				//CustomMirror.clock.xPos = x;
				//CustomMirror.clock.yPos = y;
				CustomMirror.setClock(x, y);
				console.log("Inside the custom directive and y is " + CustomMirror.clock.yPos);
			}


			// Draggabilly (makes elements draggable)
			$(document).ready( function() {
			  var $clock_drag = $('.clock').draggabilly({
			    // contain to parent element
			    containment: '.inContain'
			  });


			  //Calls listener function anytime the clock view is dragged
			  $clock_drag.on('dragMove', clockListener);

				});
			}
		};
	}])
	.directive("newsDragDrop", ["CustomMirror", function(CustomMirror) {//Test function, only for getting the x and y coordinates of elements when you drag them
		return {
		restrict: 'E',
		transclude: true,
		//scope: {},
		controller: ['$http', '$scope', function NewsController($http, $scope) {

		  var vm = this;
		  $scope.name = "Robby"; //for testing purposes
		  var URL = 'https://newsapi.org/v1/articles';

		  var request = {
		    method: 'GET',
		    url: URL,
		    params: {
		       source: 'google-news',
		       apiKey: '900a4aa086544aeba119d17207f639d3'
		    }
		  };

		  $http(request)
		    .then(function(response) {

		      $scope.newsdata = response.data;
		    }).
		    catch(function(response) {
		      $scope.newsdata = response.data;
		    });
		}],
		templateUrl: "mirror/customnews/news.html",
		link: function link(scope, element, attrs) {
			function newsListener(){
				var drag = $(this).data('draggabilly');
				var x = drag.position.x;
				var y = drag.position.y;

				//CustomMirror.news.xPos = x;
				//CustomMirror.news.yPos = y;
				CustomMirror.setNews(x, y);

			}


			// Draggabilly (makes elements draggable)
			$(document).ready( function() {
			  var $news_drag = $('.news').draggabilly({
			    // contain to parent element
			    containment: '.inContain'
			  });


			  //Calls listener function anytime the clock view is dragged
			  $news_drag.on('dragMove', newsListener);

			});
		}
	};
}])

	.directive("weatherDragDrop", ["CustomMirror", function(CustomMirror) {//Test function, only for getting the x and y coordinates of elements when you drag them
		return {
		restrict: 'E',
		transclude: true,
		//scope: {},
		controller: ['$http', '$scope', function WeatherController($http, $scope) {

		  var vm = this;
		  $scope.name = "Robby"; //for testing purposes
		  var URL = 'http://api.openweathermap.org/data/2.5/weather';

		  var request = {
		    method: 'GET',
		    url: URL,
		    params: {
		       id: '4110486', //Fayetteville, AR
		      mode: 'json',
		      units: 'imperial',
		      cnt: '7',
		      appid: '30adf3d9da7f625fea9e433f95d2364f'
		    }
		  };

		  $http(request)
		    .then(function(response) {

		      $scope.weatherdata = response.data;
		    }).
		    catch(function(response) {
		      $scope.weatherdata = response.data;
		    });
		}],
		templateUrl: "mirror/customweather/weather.html",
		link: function link(scope, element, attrs) {
			function weatherListener(){
				var drag = $(this).data('draggabilly');
				var x = drag.position.x;
				var y = drag.position.y;
				console.log('dragMove happened', x, y);
				//CustomMirror.weather.xPos = x;
				//CustomMirror.weather.yPos = y;
				CustomMirror.setWeather(x, y);

			}


			// Draggabilly (makes elements draggable)
			$(document).ready( function() {
			  var $weather_drag = $('.weather').draggabilly({
			    // contain to parent element
			    containment: '.inContain'
			  });


			  //Calls listener function anytime the clock view is dragged
			  $weather_drag.on('dragMove', weatherListener);

			});
		}
	};
}]);
