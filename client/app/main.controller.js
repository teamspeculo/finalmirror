angular
		.module('app')
		.controller('MainController', ["$scope", "$http", "$location", "$sessionStorage", "User",

		function MainController($scope, $http, $location, $sessionStorage, User)
		{
			$scope.isLoggedIn = function()
			{
				if(User.logged_in)
				{
					return true;
				}
				else
				{
					return false;
				}
			}



			$scope.logout = function()
			{
			
				$http.get('/logout').then(function(res){

					if(res.status == 200)
					{
						console.log("We logged out");
						User.email = '';
						User.logged_in = false;
						$sessionStorage.empty();
						$location.path('/');
					}
				})
				

			}

		}]);
