//Getting the main app module and adding a clock controller
angular
	.module('app')
	.controller('ClockController', ClockController)

function ClockController($scope, $interval) 
{
	//var t = this;
	$scope.name = "Ryan"
    var tick = function(){
      $scope.clock = Date.now();
    }
    tick();
    $interval(tick, 1000);
}