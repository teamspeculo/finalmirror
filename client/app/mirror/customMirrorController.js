angular
		.module('app')
		.controller('CustomMirrorController', ["$http", "$scope", "$resource", "User", "CustomMirror",
		function CustomMirrorController($http, $scope, $resource, User, CustomMirror) 
		{
			$scope.test = "Ryan";

			console.log("i'm in the CustomMirrorController and the test var is " + CustomMirror.test);
			var CustomizeMirror = $resource('mirror/update');
			var UsersCustomMirror = $resource('mirror/getMirrors/:user', 
				{user:'@user'});
			UsersCustomMirror.get({user:User.email}, function(mirror) {
						$scope.weatheron = mirror.weather.on;
						console.log("Weatheron is " + $scope.weatheron);
						console.log("mirror.weather.on is " + mirror.weather.on);
						$scope.newson = mirror.news.on;
						$scope.clockon = mirror.clock.on;
						var weatherx = mirror.weather.xPos;
						var weathery = mirror.weather.yPos;


						
					
						var newsx = mirror.news.xPos;
						var newsy = mirror.news.yPos;
						console.log("Service is getting instantiated" + newsx + " " + newsy);
				
						var clockx = mirror.clock.xPos;
						var clocky = mirror.clock.yPos;

						CustomMirror.setClock(clockx, clocky);
						CustomMirror.setNews(newsx, newsy);
						CustomMirror.setWeather(weatherx, weathery);


				});


			
			$scope.submit = function()
			{
				
				var customize = new CustomizeMirror();
			
				customize.weatheron = $scope.weatheron;
				console.log("The custommirror clock x is " + CustomMirror.clock.xPos);
				customize.weatherx = CustomMirror.weather.xPos;
				customize.weathery = CustomMirror.weather.yPos;

				customize.newson = $scope.newson;
				console.log("Newsx is " + CustomMirror.news.xPos);
				customize.newsx = CustomMirror.news.xPos;
				customize.newsy = CustomMirror.news.yPos;

				customize.clockon = $scope.clockon;
				customize.clockx = CustomMirror.clock.xPos;
				customize.clocky = CustomMirror.clock.yPos;

				customize.$save(function(putResponseHeaders)
				{
					if(putResponseHeaders.status != 200)
					{
						console.log("Couldn't save");
					}
				});
			}


		}]);
