angular.module('mirrorApp').controller('GenController', GenController)

function GenController($http, $scope, $interval)
{

	// Get serial number
	var theQuery = window.location.search;
	var idx = theQuery.indexOf("serial");
	var serial = theQuery.substring(idx+7, theQuery.length);

	$scope.serial = serial;

	var URL = 'getMirror/' + serial;
	var localRequest = {
		method: 'GET',
		url: URL,
	};

  var update = function(){
		//pickup data
		$http(localRequest).then(function(res) {
				$scope.mirror = res.data;
				$scope.showC = res.data.clock.on;
				$scope.showW = res.data.weather.on;
				$scope.showN = res.data.news.on;

				// set position of elements using jquery
				$('.clock').css({ top: $scope.mirror.clock.yPos +  'px' });
				$('.clock').css({ left: $scope.mirror.clock.xPos + 'px' });
				$('.weather').css({ top: $scope.mirror.weather.yPos + 'px' });
				$('.weather').css({ left: $scope.mirror.weather.xPos + 'px' });
				$('.news').css({ top: $scope.mirror.news.yPos + 'px' });
				$('.news').css({ left: $scope.mirror.news.xPos + 'px' });
		})
  }
  update();
  $interval(update, 1000);
}


angular.module('mirrorApp').controller('ClockController', ClockController)

function ClockController($http, $scope, $interval)
{
    var tick = function(){
      $scope.clock = Date.now();
    }
    tick();
    $interval(tick, 1000);
}

angular.module('mirrorApp').controller('WeatherController', WeatherController)

function WeatherController($http, $scope, $interval) {

  var vm = this;
  $scope.name = "Robby"; //for testing purposes
  URL = 'http://api.openweathermap.org/data/2.5/weather';

  var request = {
    method: 'GET',
    url: URL,
    params: {
       id: '4110486', //Fayetteville, AR
      mode: 'json',
      units: 'imperial',
      cnt: '7',
      appid: '30adf3d9da7f625fea9e433f95d2364f'
    }
  };

  $http(request)
    .then(function(response) {
      $scope.weatherdata = response.data;
    }).
    catch(function(response) {
      $scope.weatherdata = response.data;
    });

};

angular.module('mirrorApp').controller('NewsController', NewsController)

function NewsController($http, $scope) {

  var vm = this;
  $scope.name = "Robby"; //for testing purposes
  var URL = 'https://newsapi.org/v1/articles';

  var request = {
    method: 'GET',
    url: URL,
    params: {
       source: 'google-news',
       apiKey: '900a4aa086544aeba119d17207f639d3'
    }
  };

  $http(request)
    .then(function(response) {
      $scope.newsdata = response.data;
    }).
    catch(function(response) {
      $scope.newsdata = response.data;
    });
};
