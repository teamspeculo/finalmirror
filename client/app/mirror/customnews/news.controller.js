angular
	.module('app')
	.controller('NewsController', NewsController)

function NewsController($http, $scope) {
  var vm = this;
  $scope.name = "Robby"; //for testing purposes
  var URL = 'https://newsapi.org/v1/articles';

  var request = {
    method: 'GET',
    url: URL,
    params: {
       source: 'google-news',
       apiKey: '900a4aa086544aeba119d17207f639d3'
    }
  };

  $http(request)
    .then(function(response) {
      $scope.data = response.data;
    }).
    catch(function(response) {
      $scope.data = response.data;
    });
};
