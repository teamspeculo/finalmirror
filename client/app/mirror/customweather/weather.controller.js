angular
	.module('app')
	.controller('WeatherController', WeatherController)

function WeatherController($http, $scope) {
  var vm = this;
  $scope.name = "Robby"; //for testing purposes
  var URL = 'http://api.openweathermap.org/data/2.5/weather';

  var request = {
    method: 'GET',
    url: URL,
    params: {
       id: '4110486', //Fayetteville, AR
      mode: 'json',
      units: 'imperial',
      cnt: '7',
      appid: '30adf3d9da7f625fea9e433f95d2364f'
    }
  };

  $http(request)
    .then(function(response) {
      $scope.data = response.data;
    }).
    catch(function(response) {
      $scope.data = response.data;
    });
};
