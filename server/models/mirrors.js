var mongoose = require('mongoose');

// define the schema for our mirror model
var mirrorSchema = mongoose.Schema({

      serial        : String,
      user       : String,
      weather		 : {
        on   : Boolean,
        xPos : Number,
        yPos : Number
      },
      news		 : {
        on   : Boolean,
        xPos : Number,
        yPos : Number
      },
      clock		 : {
        on   : Boolean,
        xPos : Number,
        yPos : Number
      }
});

var Mirror = module.exports = mongoose.model('Mirror', mirrorSchema);

module.exports.createMirror = function(newMirror, callback) {
  var aMirror = new Mirror(newMirror);

  aMirror.save(callback);
}

module.exports.findMirror = function(aserial, callback) {
  var aMirror = Mirror.findOne({
    serial : aserial
  }).exec(callback);
}


module.exports.findUserMirror = function(tUser, callback) {
  var aMirror = Mirror.findOne({
    user : tUser
  }, function(err, mirror){
     if(err) return handleError(err);
     callback(mirror);
  });
}

module.exports.updateMirror = function(upMirror, callback) {
  
  Mirror.update({serial: upMirror.serial}, upMirror, {}, callback);
}

module.exports.updateUserMirror = function(upMirror, callback) {
  Mirror.update({user: upMirror.user}, upMirror, {}, callback);
}
