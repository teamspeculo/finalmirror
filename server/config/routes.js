var path = require('path');


module.exports = function(app, passport, Mirror) {
    // console.log("Oh its me");
    // Home page
    app.get('', function(req, res) {
        res.sendfile(path.resolve('/../../client/app/index.html'));
    });

    app.get('/mirror', function(req, res) {
        res.sendfile(path.resolve('../client/app/mirror/mirror.html'));
    });

    app.get('/mirrorbuild', function(req, res) {
        res.sendfile(path.resolve('../client/app/mirror/mirrorbuild.html'));
    });

    app.get('/mirror/all', function(req, res) {
        Mirror.find(function(err, mirrors) {
          if(err)
            res.send(err)
          res.json(mirrors);
        });
    });

    app.post('/mirror/create', function(req, res) {
      var tMirror = {
        serial   : req.body.serial,
        user     : req.body.user,
        weather	 : {
          on   : req.body.weather.on,
          xPos : req.body.weather.xPos,
          yPos : req.body.weather.yPos
        },
        news		 : {
          on   : req.body.news.on,
          xPos : req.body.news.xPos,
          yPos : req.body.news.yPos
        },
        clock		 : {
          on   : req.body.clock.on,
          xPos : req.body.clock.xPos,
          yPos : req.body.clock.yPos
        }
      };

      Mirror.createMirror(tMirror, function(){
        res.send({msg:''});
      });

    });

    app.post('/mirror/update', function(req, res) {
      Mirror.findUserMirror(req.user.local.email, function(doc){
      var tMirror = {
        serial   : doc.serial,
        weather	 : {
          on   : req.body.weatheron,
          xPos : req.body.weatherx,
          yPos : req.body.weathery
        },
        news		 : {
          on   : req.body.newson,
          xPos : req.body.newsx,
          yPos : req.body.newsy
        },
        clock		 : {
          on   : req.body.clockon,
          xPos : req.body.clockx,
          yPos : req.body.clocky
        }
      };
      // console.log("mirror recieved");
      // console.log(req.body);
      // console.log("mirror altered");
      // console.log(tMirror);
      Mirror.updateMirror(tMirror, function(){
        console.log("DONE WITH UPDATE");
        res.send(200);
      });
  });

    });

    app.get('/mirror/getMirror/:serial', function(req, res) {
        var serial = req.params.serial;
        Mirror.findMirror(serial, function(err, result){
            res.send(result);
        })
    });

    app.get('/mirror/getMirrors/:user', function(req, res) {

        Mirror.findUserMirror(req.params.user, function(result){

            res.json(result);
        })
    });

    app.get('/logout', function(req, res) {
        console.log("We logout");
        req.logout();
        req.session.destroy(function(err) {
          console.log("We just destroyed the sessions");
          res.send(200);
        })

    })

    // Login page
    app.post('/login', passport.authenticate('local-login'),
        function(req, res) {
	    console.log("logged in");
            res.send(req.user);

    });

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup'),
        function(req, res) {
            res.send(req.user);
        });
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}
